/* valid curses attributes are listed below they can be ORed
 *
 * A_NORMAL        Normal display (no highlight)
 *
 * A_STANDOUT      Best highlighting mode of the terminal.
 *
 * A_UNDERLINE     Underlining
 *
 * A_REVERSE       Reverse video
 *
 * A_BLINK         Blinking
 *
 * A_DIM           Half bright
 *
 * A_BOLD          Extra bright or bold
 *
 * A_PROTECT       Protected mode
 *
 * A_INVIS         Invisible or blank mode
 */

enum {
	DEFAULT,
	BLUE,
	PINK,
	RED,
};

static Color colors[] = {
	[DEFAULT] = { .fg = -1,         .bg = -1, .fg256 = -1, .bg256 = -1, },
	[BLUE]    = { .fg = COLOR_BLUE, .bg = -1, .fg256 = 45, .bg256 = -1, },

	[PINK]    = { .fg = PINK, .bg = -1, .fg256 = 198, .bg256 = -1, },
	[RED]     = { .fg = RED, .bg = -1, .fg256 = 160, .bg256 = -1, },
};

#define COLOR(c)        COLOR_PAIR(colors[c].pair)

/* curses attributes for the currently focused window */
#define SELECTED_ATTR   (COLOR(BLUE) | A_NORMAL)

/* curses attributes for normal (not selected) windows */
#define NORMAL_ATTR     (COLOR(PINK) | A_NORMAL)

/* curses attributes for a window with pending urgent flag */
#define URGENT_ATTR     NORMAL_ATTR

/* curses attributes for the status bar */
#define BAR_ATTR        (COLOR(BLUE) | A_NORMAL)

/* characters for beginning and end of status bar message */
#define BAR_BEGIN       '['
#define BAR_END         ']'

/* status bar (command line option -s) position */
#define BAR_POS         BAR_BOTTOM /* BAR_BOTTOM, BAR_OFF */

/* whether status bar should be hidden if only one client exists */
#define BAR_AUTOHIDE    false

/* master width factor [0.1 .. 0.9] */
#define MFACT 0.5

/* number of clients in master area */
#define NMASTER 1

/* scroll back buffer size in lines */
#define SCROLL_HISTORY 10000

/* printf format string for the tag in the status bar */
#define TAG_SYMBOL   "[%s]"

/* curses attributes for the currently selected tags */
#define TAG_SEL      (COLOR(BLUE) | A_BOLD)

/* curses attributes for not selected tags which contain no windows */
#define TAG_NORMAL   (COLOR(PINK) | A_NORMAL)

/* curses attributes for not selected tags which contain windows */
#define TAG_OCCUPIED (COLOR(PINK) | A_NORMAL)

/* curses attributes for not selected tags which with urgent windows */
#define TAG_URGENT (COLOR(RED) | A_NORMAL | A_BLINK)

/* tagging                main    programming asocial media   gaming      5     6     7     8     virtual machines*/
const char tags[][20] = { "主要", "編程", "無社會性", "媒體", "電子遊戲", "五", "六", "七", "八", "虛擬機器" };

#include "layouts.c"
#include "fullscreen.c"

#include "vstack.c"

/* by default the first layout entry is used */
static Layout layouts[] = {
	{ "[+]", grid },
	{ "[-]", vstack },
	{ "[ ]", fullscreen },
};

#define MOD  CTRL('a')
#define TAGKEYS(KEY,TAG) \
	{ { MOD, 'v', KEY,     }, { view,           { tags[TAG] }               } }, \
	{ { MOD, 'V', KEY,     }, { toggleview,     { tags[TAG] }               } }, \
	{ { MOD, 't', KEY,     }, { tag,            { tags[TAG] }               } }, \
	{ { MOD, 'T', KEY,     }, { toggletag,      { tags[TAG] }               } },

/* you can at most specifiy MAX_ARGS (3) number of arguments */
static KeyBinding bindings[] = {
	{ { MOD, 'o',          }, { create,         { NULL }                    } },
	{ { MOD, 'O',          }, { create,         { NULL, NULL, "$CWD" }      } }, /* creates a new window with CWD, maybe remove this */

	{ { MOD, 'Q',          }, { quit,           { NULL }                    } },
	{ { MOD, 'P',          }, { killclient,     { NULL }                    } },

	{ { MOD, 'j',          }, { focusnext,      { NULL }                    } },
	{ { MOD, 'k',          }, { focusprev,      { NULL }                    } },

	//{ { MOD, 'J',          }, { focusnextnm,    { NULL }                    } },
	//{ { MOD, 'K',          }, { focusprevnm,    { NULL }                    } },

	{ { MOD, 'J',          }, { pushdown,       { NULL }                    } },
	{ { MOD, 'K',          }, { pushup,         { NULL }                    } },

	{ { MOD, '\t'          }, { viewprevtag,    { NULL }                    } },
	{ { MOD, ' ',          }, { setlayout,      { NULL }                    } },

	{ { MOD, 'm',          }, { toggleminimize, { NULL }                    } },
	{ { MOD, 's',          }, { togglebar,      { NULL }                    } },

	{ { MOD, CTRL('u'),    }, { scrollback,     { "-1" }                    } }, /* just use ctrl u */
	{ { MOD, CTRL('d'),    }, { scrollback,     { "1"  }                    } }, /* just use ctrl d */







	{ { MOD, '1',          }, { view,           { tags[0] }                 } },
	{ { MOD, '2',          }, { view,           { tags[1] }                 } },

	{ { MOD, '3',          }, { view,           { tags[2] }                 } },
	{ { MOD, '4',          }, { view,           { tags[3] }                 } },

	{ { MOD, '5',          }, { view,           { tags[4] }                 } },
	{ { MOD, '6',          }, { view,           { tags[5] }                 } },

	{ { MOD, '7',          }, { view,           { tags[6] }                 } },
	{ { MOD, '8',          }, { view,           { tags[7] }                 } },

	{ { MOD, '9',          }, { view,           { tags[8] }                 } },
	{ { MOD, '0',          }, { view,           { tags[9] }                 } },

	/* add shift mask so I can move windows using MOD + Shift + numbers */
//	{ { MOD, '1',          }, { tag,            { tags[0] }                 } },
//	{ { MOD, '2',          }, { tag,            { tags[1] }                 } },

	TAGKEYS( '1',                              0)
	TAGKEYS( '2',                              1)

	TAGKEYS( '3',                              2)
	TAGKEYS( '4',                              3)

	TAGKEYS( '5',                              4)
	TAGKEYS( '6',                              5)

	TAGKEYS( '7',                              6)
	TAGKEYS( '8',                              7)

	TAGKEYS( '9',                              8)
	TAGKEYS( '0',                              9)


	{ { MOD, 'S',          }, { togglebarpos,   { NULL }                    } }, /* mayeb remove this? May come in handy, figure out where bar should be then decide*/
	{ { MOD, 'i',          }, { zoom ,          { NULL }                    } }, /* REMOVE ME */

	{ { MOD, 'I',          }, { zoom ,          { NULL }                    } }, /* REMOVE ME */

	{ { MOD, CTRL('L'),    }, { redraw,         { NULL }                    } }, /* maybe remove this? */
	{ { MOD, 'r',          }, { redraw,         { NULL }                    } }, /* maybe remove this? */

	{ { MOD, 'e',          }, { copymode,       { NULL }                    } }, /* how does this work!? */
	{ { MOD, '/',          }, { copymode,       { "/" }                     } }, /* how does this work!? */

	{ { MOD, 'p',          }, { paste,          { NULL }                    } },
	{ { MOD, '?',          }, { create,         { "man dvtm", "dvtm help" } } }, /* remove this */

	{ { MOD, MOD,          }, { send,           { (const char []){MOD, 0} } } }, /* REMOVE ME */

	{ { KEY_SPREVIOUS,     }, { scrollback,     { "-1" }                    } }, /* just a copy? */
	{ { KEY_SNEXT,         }, { scrollback,     { "1"  }                    } }, /* just a copy? */
};

static const ColorRule colorrules[] = {
	{ "", A_NORMAL, &colors[DEFAULT] }, /* default */
};

static Cmd commands[] = {
	{ "create", { create,	{ NULL } } },
};

/* gets executed when dvtm is started */
static Action actions[] = {
	{ create, { NULL } },
};

static char const * const keytable[] = {
	/* add your custom key escape sequences */
};

/* editor to use for copy mode. If neither of DVTM_EDITOR, EDITOR and PAGER is
 * set the first entry is chosen. Otherwise the array is consulted for supported
 * options. A %d in argv is replaced by the line number at which the file should
 * be opened. If filter is true the editor is expected to work even if stdout is
 * redirected (i.e. not a terminal). If color is true then color escape sequences
 * are generated in the output.
 */
static Editor editors[] = {
	{ .name = "vis",         .argv = { "vis", "+%d", "-", NULL   }, .filter = true,  .color = false },
	{ .name = "sandy",       .argv = { "sandy", "-d", "-", NULL  }, .filter = true,  .color = false },
	{ .name = "dvtm-editor", .argv = { "dvtm-editor", "-", NULL  }, .filter = true,  .color = false },
	{ .name = "vim",         .argv = { "vim", "+%d", "-", NULL   }, .filter = false, .color = false },
	{ .name = "less",        .argv = { "less", "-R", "+%d", NULL }, .filter = false, .color = true  },
	{ .name = "more",        .argv = { "more", "+%d", NULL       }, .filter = false, .color = false },
};
