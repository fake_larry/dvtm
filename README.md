# Changes:

* fix grid() layout (doesn't show border on horizontal splits)

* change keybindings to mirror vortex and TMUX setup

* update status bar to be like vortex

* MOD + shift + 1-0 move focused window to specified tag

* add pertag
